return function(B, H)
return {
	imap = {
	-- Structured motions:
		['<M-C-f>'] = B.M.forward,
		['<M-C-b>'] = B.M.backward,
		['<M-C-d>'] = B.M.forward_down,
		['<M-C-u>'] = B.M.backward_up,
		['<M-C-n>'] = B.M.forward_up,
		['<M-C-p>'] = B.M.backward_down,
		['<M-C-e>'] = B.M.next_beginning_of_defun,
		['<M-C-a>'] = B.M.prev_beginning_of_defun,
		['<M-e>']   = B.M.end_of_sentence,
		['<M-a>']   = B.M.beginning_of_sentence,
		['<M-f>']   = B.M.next_finish_float,
		['<M-b>']   = B.M.prev_start_float,
		['<C-x>]']  = B.M.next_section,
		['<C-x>[']  = B.M.prev_section,
	-- Structured selections:
		['<M-C- >'] = H.I.mark_sexp,
		['<M-C-h>'] = H.I.mark_defun,
		['<M-h>']   = H.I.mark_defun,
		['<M-=>']   = H.I.expand_region,
	-- Structured deletions:
		['<M-C-k>'] = H.I.kill_sexp,
		['<C-w>']   = H.I.backward_kill_sexp,  -- kill-region, if there is a selection
		['<M-k>']            = H.I.kill_sentence,
		['<C-x><Backspace>'] = H.I.backward_kill_sentence,
		['<M-d>']         = H.I.forward_kill_word,
		['<M-Backspace>'] = H.I.backward_kill_word,
		['<C-k>']      = H.I.kill,
		['<M-0><C-k>'] = H.I.backward_kill_line,
	-- Structured operators:
		['<M-S>'] = B.O.split_sexp,
		['<M-J>'] = B.O.join_sexps,
		['<C-Left>']    = B.O.forward_barf,
		['<C-Right>']   = B.O.forward_slurp,
		['<M-C-Right>'] = B.O.backward_barf,
		['<M-C-Left>']  = B.O.backward_slurp,
		['<M-r>'] = B.O.raise_sexp,
		['<M-?>'] = B.O.convolute_sexp,
		['<M-s>'] = B.O.splice_sexp,
		['<M-(>'] = B.O.wrap_round,
		['<M-">'] = B.O.meta_doublequote,
		['<M-;>'] = B.O.wrap_comment,
		['<M-C-t>'] = B.O.transpose_sexps,
		['<M-t>']   = B.O.transpose_words,
		['<M-Down>'] = H.I.splice_sexp_killing_forward,
		['<M-Up>']   = H.I.splice_sexp_killing_backward,
	-- General operators:
		['<C-d>']       = H.I.forward_delete,
		['<Delete>']    = H.I.forward_delete,
		['<Backspace>'] = H.I.backward_delete,
		['<M-w>'] = B.O.yank,  -- kill-ring-save
		['<C-y>'] = B.O.put_after,  -- yank
		['<C- >'] = H.I.set_mark_command,
		['<C-x><C-x>'] = H.I.exchange_point_and_mark,
		['<C-_>']      = H.A.undo,
		['<C-g><C-_>'] = H.A.redo,
		['<M-q>'] = B.O.format,  -- reindent-defun
		['<C-t>'] = B.O.transpose_chars,
		['<M-Enter>'] = B.O.open_next_line,
		['<M-)>'] = B.O.close_and_newline,
		['<M-]>'] = B.O.close_and_newline,
		['<M-}>'] = B.O.close_and_newline,
		['<C-x><C-e>'] = H.I.eval_last_sexp,
		['<M-C-x>']    = B.O.eval_defun,
	},
	raw = function(win)
		local vis = vis
		local function local_set_key(key, binding)
			win:map(vis.modes.INSERT, key, binding)
		end
		local_set_key('<C-f>', '<vis-motion-char-next>')
		local_set_key('<C-b>', '<vis-motion-char-prev>')
		local_set_key('<C-n>', '<vis-motion-line-down>')  -- this disables completion
		local_set_key('<C-p>', '<vis-motion-line-up>')
		local_set_key('<C-e>', '<vis-motion-line-end>')
		local_set_key('<C-a>', '<vis-motion-line-begin>')
		local_set_key('<M-m>', '<vis-motion-line-start>')
		local_set_key('<M->>', '<vis-motion-line-last><vis-append-line-end>')
		local_set_key('<M-<>', '<vis-motion-line-first>')
		local_set_key('<C-x><C-s>', '<vis-prompt-show>:w<Enter><vis-mode-insert>')  -- XXX: upstream vis can't handle C-s and C-q
		local_set_key('<C-x><C-c>', function() vis:exit(0) end)
	end
}
end
