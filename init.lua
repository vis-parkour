-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local M = {
	auto_square_brackets = false,
	lispwords = {},
	repl_fifo = false,
	emacs = false,
	autoselect = false,
}

require("vis")
local vis = vis

-- XXX: in Lua 5.2 unpack() was moved into table
local unpack = table.unpack or unpack

local env = {}
local keycfg = {}

local cwd = ...
local init, supports = unpack((require(cwd..".parkour")))

local do_copy = false
local insert_mode = false
local mark

local H = {M = {}, T = {}, O = {}, I = {}, A = {}}  -- handler functions
local R = {M = {}, T = {}, O = {}}  -- registered handler IDs
local B = {M = {}, T = {}, O = {}}  -- bindings for registered handlers
local new = {}  -- constructors

-- IDs of built-in motions and textobjects used in the plugin (copied from vis.h)
local VIS_MOVE_CHAR_PREV = 16
local VIS_MOVE_CHAR_NEXT = 17
local VIS_MOVE_NOP = 64
local VIS_TEXTOBJECT_OUTER_LINE = 23

setmetatable(M, {__newindex = function(_, k)
	local options = {}
	for option, _ in pairs(M) do
		table.insert(options, option)
	end
	table.sort(options)
	vis:message(([[%s: unrecognized option '%s'
	 Possible values: %s]]):format(cwd, k, table.concat(options, ", ")))
end})

setmetatable(M.lispwords, {__index = function(t, filetype)
	if supports[filetype] then
		local recfg = {__newindex = function(_, k, v)
			vis.events.subscribe(vis.events.WIN_OPEN, function(win)
				if filetype == win.syntax then
					env[win].lispwords[k] = v
				end
			end)
		end}
		t[filetype] = setmetatable({}, recfg)
		return t[filetype]
	end
end})

local function win_map(win, kind, key, action)
	if kind == "T" then
		win:map(vis.modes.OPERATOR_PENDING, key, action)
		if not M.autoselect then
			win:map(vis.modes.VISUAL, key, action)
		end
	elseif kind == "O" then
		win:map(vis.modes.NORMAL, key, action)
		win:map(vis.modes.VISUAL, key, action)
	elseif kind == "M" then
		win:map(vis.modes.NORMAL, key, action)
		win:map(vis.modes.OPERATOR_PENDING, key, action)
		win:map(vis.modes.VISUAL, key, action)
	end
end

local function mark_active() return insert_mode and mark end

local function iprep(func, win)
	return function()
		insert_mode = true
		func(win)
		insert_mode = false
	end
end

local function imap(win, key, action)
	win:map(vis.modes.INSERT, key, iprep(action, win))
end

local function rewind_on(action)
	return function()
		local sequence = "<vis-"..action..">"
		if vis.mode == vis.modes.VISUAL then
			sequence = sequence .. "<vis-selections-remove-all>"
		end
		vis:feedkeys(sequence)
		-- force a reparse from the very beginning, as undo/redo can create or remove
		-- multiple paragraphs in one go and I don't know how much to rewind.
		env[vis.win].parser.tree.rewind(0)
	end
end

H.A.undo = rewind_on("undo")
H.A.redo = rewind_on("redo")

function H.A.select_sexp()
	vis.mode = vis.modes.VISUAL
	vis:textobject(R.T.inner_sexp)
end

local function vinsert(win, append)
	for selection in win:selections_iterator() do
		selection.pos = append and selection.range.finish or selection.range.start
	end
	vis.mode = vis.modes.INSERT
end

local function backslash(keys)
	local win = vis.win
	local sel_is_escaped = {}
	local range = {}
	local any_sel_in_code = false
	for i = #win.selections, 1, -1 do
		local selection = win.selections[i]
		range.start, range.finish = selection.pos, selection.pos
		sel_is_escaped[i] = env[win].walker.escaped_at(range)
		if not sel_is_escaped[i] or not sel_is_escaped[i].is_comment then
			any_sel_in_code = true
		end
	end
	if #keys < 1 and any_sel_in_code then vis:info("Character to escape: ") return -1 end
	for i = #win.selections, 1, -1 do
		local selection = win.selections[i]
		local pos = selection.pos
		local prefix = "\\"
		win.file:insert(pos, prefix..keys)
		selection.pos = pos + #(prefix..keys)
		env[win].parser.tree.rewind(pos)
	end
	return #keys
end

local function range_by_pos(win, pos)
	for selection in win:selections_iterator() do
		if selection.pos == pos then
			return insert_mode and {start = pos, finish = pos} or selection.range, selection.number
		end
	end
	return {start = pos, finish = pos + (insert_mode and 0 or 1)}
end

local function selection_by_pos(win, pos)
	for selection in win:selections_iterator() do
		if selection.pos == pos or selection.range and selection.range.start == pos then
			return selection
		end
	end
end

local function autoselect_region()
	vis.mode = vis.modes.VISUAL
	vis:textobject(R.T.expand_region)
end

local last_mode

vis.events.subscribe(vis.events.WIN_STATUS, function(win)
	if win ~= vis.win then return end
	if not vis_parkour(win) then return end
	if last_mode == vis.mode then return end
	if vis.mode == vis.modes.NORMAL and last_mode == vis.modes.INSERT then
		mark = nil
	end
	last_mode = vis.mode
end)

vis.events.subscribe(vis.events.WIN_HIGHLIGHT, function(win)
	if win ~= vis.win then return end
	if not vis_parkour(win) then return end
	if vis.mode == vis.modes.INSERT and mark then
		local m = win.file:mark_get(mark)
		local p = win.selection.pos
		win:style(win.STYLE_CURSOR, math.min(m, p), math.max(m, p) - 1)
	end
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	if not vis_parkour(win) then return end
	local function write(pos, txt)
		mark = nil
		return win.file:insert(pos, txt)
	end
	local function delete(pos, len)
		mark = nil
		return win.file:delete(pos, len)
	end
	local function read(base, len)
		if base and base >= win.file.size then return end
		base = base or 0
		len = len or win.file.size
		local more = base + len < win.file.size
		return win.file:content(base, len), more
	end
	local function eol_at(pos)
		local sel = selection_by_pos(win, pos)
		if sel then
			local line_start = sel.pos - sel.col + 1
			return line_start + #win.file.lines[sel.line]
		else
			-- XXX: bad, we have to do ad hoc reads on the file.
			-- This fixes close_and_newline() and open_next_line() not opening an empty line
			-- as intended, because this wrapper only worked for `pos` equal to that of an
			-- existing selection. The underlying library sometimes needs to use it for
			-- arbitrary positions, though. The same applies to bol_at().
			local horizon = 80
			local line_end = win.file:content(pos, pos + horizon):find"\n"
			return pos + line_end - 1
		end
	end
	local function bol_at(pos)
		local sel = selection_by_pos(win, pos)
		if sel then
			return sel.pos - sel.col + 1
		end
	end
	env[win] = init(win.syntax, read, write, delete, eol_at, bol_at)
	local consumed_char = {"\n", " "}
	for ch in pairs(env[win].parser.opposite) do
		if #ch == 1 then
			table.insert(consumed_char, ch)
		end
	end
	env[win].consumed_char = "[" .. table.concat(consumed_char, "%") .. "]"
	for _, setup in pairs(keycfg) do
		for kind, bindings in pairs(setup) do
			if kind == "imap" then
				for key, action in pairs(bindings) do
					imap(win, key, action)
				end
			elseif kind == "nmap" then
				for key, action in pairs(bindings) do
					win:map(vis.modes.NORMAL, key, action)
				end
			elseif type(bindings) == "table" then
				for key, action in pairs(bindings) do
					win_map(win, kind, key, action)
				end
			end
		end
		if setup["raw"] then
			setup["raw"](win)
		end
	end
	win:map(vis.modes.INSERT, "<Enter>", function() vis:feedkeys("\n") end)
	win:map(vis.modes.INSERT, "\\", backslash)
	if M.autoselect then
		win:unmap(vis.modes.VISUAL, "i")
		win:unmap(vis.modes.VISUAL, "a")
		-- make sure there are no mappings that start with i or a,
		-- otherwise switching from visual straight to insert mode won't work:
		local bindings = vis:mappings(vis.modes.VISUAL)
		for key in pairs(bindings) do
			if key:sub(1, 1):match("[ia]") then
				vis:unmap(vis.modes.VISUAL, key)
			end
		end
		win:map(vis.modes.VISUAL, "i", function() vinsert(win) end)
		win:map(vis.modes.VISUAL, "a", function() vinsert(win, true) end)
		win:map(vis.modes.NORMAL, "v", autoselect_region)
	end
	win:map(vis.modes.VISUAL, "v", function()
		vis:textobject(R.T.expand_region)
	end)
	vis:command("set expandtab")
end)

local function is_comment(t) return t.is_comment end
local function startof(t) return t.start end

function H.M.prev_start(win, range)
	return env[win].walker.start_before(range, is_comment)
end

function H.M.prev_start_float(win, range)
	-- XXX: ignore the second value:
	return (env[win].walker.start_float_before(range))
end

function H.M.next_start(win, range)
	return env[win].walker.start_after(range, is_comment)
end

function H.M.next_start_float(win, range)
	-- XXX: ignore the second value:
	return (env[win].walker.start_float_after(range))
end

function H.M.prev_finish(win, range, _, exclusive)
	local newpos = env[win].walker.finish_before(range, is_comment)
	return newpos and newpos - 1 + exclusive
end

function H.M.prev_finish_float(win, range, _, exclusive)
	local newpos = env[win].walker.finish_float_before(range)
	return newpos and newpos - 1 + exclusive
end

function H.M.next_finish(win, range, _, exclusive)
	local newpos = env[win].walker.finish_after(range, is_comment)
	return newpos and newpos - 1 + exclusive
end

function H.M.next_finish_float(win, range, _, exclusive)
	local newpos = env[win].walker.finish_float_after(range)
	return newpos and newpos - 1 + exclusive
end

function H.M.forward(win, range)
	return env[win].walker.finish_after(range, is_comment)
end

function H.M.backward(win, range)
	return env[win].walker.start_before(range, is_comment)
end

function H.M.forward_up(win, range, pos, exclusive)
	-- XXX: make splice-killing-forward work even on the closing paren:
	if exclusive > 0 then
		local sexp = env[win].walker.sexp_at(range)
		local closing = env[win].parser.opposite[sexp.d]
		if sexp and sexp.finish + (closing and 1 - #closing or 0) == pos then
			range.finish = range.finish - exclusive
		end
	end
	local newpos = env[win].walker.finish_up_after(range)
	return newpos and newpos - 1 + exclusive
end

function H.M.forward_down(win, range)
	return env[win].walker.start_down_after(range, is_comment)
end

function H.M.backward_up(win, range)
	return env[win].walker.start_up_before(range)
end

function H.M.backward_down(win, range, _, exclusive)
	local newpos, list = env[win].walker.finish_down_before(range, is_comment)
	return newpos and (list.is_empty and newpos or newpos - 1 + exclusive)
end

function H.M.beginning_of_sentence(win, range)
	-- XXX: make sure we don't go up if on the closing paren:
	range.finish = range.start
	return env[win].walker.anylist_start(range)
end

function H.M.end_of_sentence(win, range)
	-- XXX: make sure delete won't splice when on the closing paren:
	range.finish = range.start
	local exclude = vis.mode == vis.modes.VISUAL and 1 or 0
	local newpos = env[win].walker.anylist_finish(range)
	return newpos and newpos - exclude
end

function H.M.next_section(win, range)
	local sexp = env[win].walker.find_after(range, function(t) return t.section and t.start > range.start end)
	return sexp and sexp.start or win.file.size
end

function H.M.prev_section(win, range)
	local sexp = env[win].walker.find_before(range, function(t) return t.section end)
	return sexp and sexp.start or 0
end

function H.M.beginning_of_defun(win, range)
	local sexp = env[win].walker.paragraph_at(range)
	return sexp and sexp.start
end

function H.M.prev_beginning_of_defun(win, range)
	local sexp = env[win].parser.tree.before(range.start, startof, is_comment)
	return sexp and sexp.start
end

function H.M.end_of_defun(win, range)
	local sexp = env[win].walker.paragraph_at(range)
	return sexp and sexp.finish + (insert_mode and 1 or 0)
end

function H.M.next_beginning_of_defun(win, range)
	local parent = env[win].walker.paragraph_at(range)
	local sexp = env[win].parser.tree.after(parent and parent.finish or range.finish, startof, is_comment)
	return sexp and sexp.start
end

function H.M.line_begin(win, range)
	return env[win].walker.prev_start_wrapped(range)
end

function H.M.line_end(win, range, _, exclusive)
	-- XXX: make sure we don't go up if on the closing paren:
	range.finish = range.start
	return env[win].walker.next_finish_wrapped(range) - 1 + exclusive
end

-- This motion is only for use with delete
function H.M.backward_word(win, range)
	-- XXX: ignore the second value:
	return (env[win].walker.start_float_before(range, true))
end

-- This motion is only for use with delete
function H.M.forward_word(win, range, _, exclusive)
	local newpos = env[win].walker.finish_float_after(range, true)
	return newpos and newpos - 1 + exclusive
end

function H.M.mark(win)
	return mark and win.file:mark_get(mark)
end

local function block_textobject(opening, inner)
	return function(win, range)
		local parent = env[win].walker.list_at(range, opening)
		if not parent or parent.is_root then return end
		local closing = env[win].parser.opposite[parent.d]
		local pstart = parent.start + (inner and (parent.p and #parent.p or 0) + #parent.d or 0)
		local pfinish = parent.finish + 1 - (inner and #closing or 0)
		return pstart, pfinish
	end
end

H.T.outer_list = block_textobject()
H.T.inner_list = block_textobject(nil, true)
H.T.outer_sentence = block_textobject(false)
H.T.inner_sentence = block_textobject(false, true)
H.T.outer_string = block_textobject('"')
H.T.inner_string = block_textobject('"', true)
H.T.outer_comment = block_textobject(";")
H.T.inner_comment = block_textobject(";", true)
H.T.outer_round = block_textobject("(")
H.T.inner_round = block_textobject("(", true)
H.T.outer_square = block_textobject("[")
H.T.inner_square = block_textobject("[", true)
H.T.outer_curly = block_textobject("{")
H.T.inner_curly = block_textobject("{", true)

function H.T.outer_sexp(win, range)
	local sexp = env[win].walker.sexp_at(range)
	if sexp then
		return sexp.start, sexp.finish + 1
	end
end

H.T.inner_sexp = H.T.outer_sexp

function H.T.outer_paragraph(win, range)
	local sexps = env[win].walker.repl_line_at(range)
	if sexps then
		return sexps.start, sexps.finish + 1
	end
end

function H.T.inner_paragraph(win, range)
	local sexp = env[win].walker.paragraph_at(range)
	if sexp and not sexp.is_line_comment then
		return sexp.start, sexp.finish + 1
	end
end

function H.T.expand_region(win, range)
	return env[win].walker.wider_than(range)
end

function H.T.outer_line(win, range)
	local sel = selection_by_pos(win, range.start)
	if not sel then return end
	local line_start = range.start - sel.col + 1
	local line_finish = line_start + #win.file.lines[sel.line]
	return line_start, line_finish + 1
end

function H.T.inner_line(win, range)
	local sel = selection_by_pos(win, range.start)
	if not sel then return end
	local _, indent = win.file.lines[sel.line]:find("^[ \t]*")
	local line_start = range.start - sel.col + 1
	local line_begin = H.M.line_begin(win, range)
	local line_end = H.M.line_end(win, range, nil, 1)
	return math.max(line_start + indent, line_begin), line_end
end

function H.I.set_mark_command()
	local start = mark and vis.win.file:mark_get(mark)
	local finish = vis.win.selection.pos
	local len = start and finish and finish - start
	if not len or len ~= 0 then
		mark = vis.win.file:mark_set(finish)
		vis:info"Mark set"
	else
		mark = nil
		vis:info("Mark unset")
	end
end

function H.I.exchange_point_and_mark()
	local anchor = mark and vis.win.file:mark_get(mark)
	if anchor then
		mark = vis.win.file:mark_set(vis.win.selection.pos)
		vis.win.selection.pos = anchor
	else
		vis:info("No mark set in this buffer")
	end
end

function H.I.mark_sexp()
	local m = mark and vis.win.file:mark_get(mark)
	local p = vis.win.selection.pos
	local range = range_by_pos(vis.win, m or p)
	local backward = m and m < p
	local newpos
	if backward then
		newpos = H.M.prev_start(vis.win, range)
	else
		newpos = H.M.next_finish(vis.win, range, m, 1)
	end
	if newpos then
		if not mark then
			vis:info"Mark set"
		end
		mark = vis.win.file:mark_set(newpos)
	end
end

function H.I.mark_defun()
	local m = mark and vis.win.file:mark_get(mark)
	local p = vis.win.selection.pos
	local backward = m and m < p
	local range = m and {start = math.min(m, p), finish = math.max(m, p)}
		or range_by_pos(vis.win, p)
	local start, finish = H.T.inner_paragraph(vis.win, range)
	if start and finish then
		vis.win.selection.pos = backward and finish or start
		mark = vis.win.file:mark_set(backward and start or finish)
		if not m then vis:info"Mark set" end
	end
end

function H.I.expand_region()
	local m = mark and vis.win.file:mark_get(mark)
	local p = vis.win.selection.pos
	local backward = m and m < p
	local range = m and {start = math.min(m, p), finish = math.max(m, p)}
		or range_by_pos(vis.win, p)
	local start, finish = H.T.expand_region(vis.win, range)
	if start and finish then
		vis.win.selection.pos = backward and finish or start
		mark = vis.win.file:mark_set(backward and start or finish)
		if not m then vis:info"Mark set" end
	end
end

function H.I.backward_delete()
	vis:operator(R.O.change)
	vis:motion(VIS_MOVE_CHAR_PREV)
end

function H.I.forward_delete()
	vis:operator(R.O.change)
	vis:motion(VIS_MOVE_CHAR_NEXT)
end

-- XXX: force vis into remembering each individual operator call in INSERT mode:

local function begin_undo_action(non_destructive)
	if insert_mode and not non_destructive then
		vis.mode = vis.modes.NORMAL
	end
end

local function end_undo_action()
	if insert_mode then
		vis.mode = vis.modes.INSERT
	end
end

local function copy_enable(func)
	return function(...)
		do_copy = true
		begin_undo_action()
		vis.registers["0"] = {""}
		func(...)
		end_undo_action()
		do_copy = false
	end
end

H.I.kill_sexp = copy_enable(function()
	vis:operator(R.O.change)
	vis:motion(R.M.next_finish)
end)

H.I.backward_kill_sexp = copy_enable(function()
	vis:operator(R.O.change)
	vis:motion(mark_active() and R.M.mark or R.M.prev_start)
end)

H.I.kill_sentence = copy_enable(function()
	vis:operator(R.O.change)
	vis:motion(R.M.end_of_sentence)
end)

H.I.backward_kill_sentence = copy_enable(function()
	vis:operator(R.O.change)
	vis:motion(R.M.beginning_of_sentence)
end)

H.I.backward_kill_line = copy_enable(function()
	vis:operator(R.O.change)
	vis:motion(R.M.line_begin)
end)

H.I.kill = copy_enable(function()
	vis:operator(R.O.change)
	vis:motion(R.M.line_end)
end)

H.I.forward_kill_word = copy_enable(function()
	vis:operator(R.O.change)
	vis:motion(R.M.forward_word)
end)

H.I.backward_kill_word = copy_enable(function()
	vis:operator(R.O.change)
	vis:motion(R.M.backward_word)
end)

H.I.splice_sexp_killing_forward = copy_enable(function()
	vis:operator(R.O.delete)
	vis:motion(R.M.forward_up)
end)

H.I.splice_sexp_killing_backward = copy_enable(function()
	vis:operator(R.O.delete)
	vis:motion(R.M.backward_up)
end)

function H.I.eval_last_sexp()
	vis:operator(R.O.eval_defun)
	vis:motion(R.M.backward)
end

local function eval_repl_line(win, range, pos)
	local bol = H.T.outer_paragraph(win, range)
	return bol and H.O.eval_defun(win.file, {start = bol, finish = pos}, pos)
end

function H.O.format(_, range, pos)
	local cursor = range_by_pos(vis.win, pos)
	-- XXX: don't call range_by_pos() from the argument list, as it returns a second value
	return env[vis.win].edit.refmt_at(range, cursor) or pos
end

local function make_yank(reg, sel_number, handler)
	return function(range)
		local txt = vis.win.file:content(range)
		if txt then
			local clipboard = vis.registers[reg]
			clipboard[sel_number] = txt .. (clipboard[sel_number] or "")
			vis.registers[reg] = clipboard
		end
		return handler and handler(range)
	end
end

local function _delete(range)
	mark = nil
	return vis.win.file:delete(range)
end

function H.O.delete(_, range, pos)
	local _, n = range_by_pos(vis.win, pos)
	local delete_and_yank = make_yank(insert_mode and do_copy and "0" or vis.register or '"', n, _delete)
	local splicing = vis.mode ~= vis.modes.VISUAL and #vis.win.selections == 1
	return env[vis.win].edit.delete_splicing(range, pos, splicing, delete_and_yank)
end

local selections = 0

function H.O.change(_, range, pos)
	local _, n = range_by_pos(vis.win, pos)
	local delete_maybe_yank = insert_mode and not do_copy and _delete or
		make_yank(insert_mode and "0" or vis.register or '"', n, _delete)
	if selections == 0 then
		selections = #vis.win.selections
	end
	selections = selections - 1
	if selections == 0 then
		vis.mode = vis.modes.INSERT
	end
	return env[vis.win].edit.delete_nonsplicing(range, pos, delete_maybe_yank)
end

function H.O.yank(_, range, pos)
	local _, n = range_by_pos(vis.win, pos)
	local yank = make_yank(insert_mode and "0" or vis.register or '"', n)
	local action = {kill = false, wrap = true, splice = false, func = yank}
	env[vis.win].edit.pick_out(range, pos, action)
	return insert_mode and pos or range.start
end

local last_object_yanked
local about_to_yank

local function textobject_includes_separator(object_type)
	return (object_type == "outer_line") or (object_type == "inner_line")
end

function H.O.put_after(file, range, pos)
	local _, n = range_by_pos(vis.win, pos)
	local clipboard = vis.registers[insert_mode and 0 or vis.register or '"'][n]
	if not clipboard or #clipboard == 0 then return pos end
	local visual = ({[vis.modes.VISUAL] = true, [vis.modes.VISUAL_LINE] = true})[vis.mode]
	local newpos = range.finish
	if visual then
		-- XXX: like H.O.delete(), but without overwriting the register content
		local action = {kill = true, wrap = false, splice = false, func = _delete}
		env[vis.win].edit.pick_out(range, pos, action)
		newpos = range.start
	end
	local on_object = not visual and last_object_yanked and (range.start < range.finish) and pos < range.finish
	if on_object then
		newpos = range.finish - (textobject_includes_separator(last_object_yanked) and 0 or 1)
	end
	local needs_separator = not visual and last_object_yanked and not textobject_includes_separator(last_object_yanked)
	local separator = ""
	if needs_separator and on_object then
		local sexp, parent = env[vis.win].walker.sexp_at(range)
		local nxt = parent.is_list and parent.after(range.finish, startof, is_comment)
		if (nxt and nxt.indent or sexp and sexp.indent and not sexp.is_line_comment) then
			separator = "\n"
		elseif not sexp.is_line_comment then
			separator = " "
		end
	end
	newpos = newpos + (needs_separator and 1 or 0)
	if last_object_yanked == "inner_line" then
		file:insert(newpos, "\n")
	end
	file:insert(newpos, ((on_object and needs_separator) and separator or "") .. clipboard)
	-- XXX: it is important NOT to "rewind forward", which would leave unparsed gaps,
	-- leading to incorrect structured actions:
	if env[vis.win].parser.tree.is_parsed(newpos) then
		env[vis.win].parser.tree.rewind(newpos)
	end
	newpos = newpos + (needs_separator and #separator or 0) + #clipboard - (insert_mode and 0 or 1)
	local r = {start = newpos, finish = newpos + (insert_mode and 0 or 1)}
	local _, parent = env[vis.win].walker.sexp_at(r, true)
	return env[vis.win].edit.refmt_at(parent, r) or newpos
end

function H.O.put_before(file, range, pos)
	local _, n = range_by_pos(vis.win, pos)
	local clipboard = vis.registers[insert_mode and 0 or vis.register or '"'][n]
	if not clipboard or #clipboard == 0 then return pos end
	local visual = ({[vis.modes.VISUAL] = true, [vis.modes.VISUAL_LINE] = true})[vis.mode]
	if visual then
		local action = {kill = true, wrap = false, splice = false, func = _delete}
		local ndeleted = env[vis.win].edit.pick_out(range, pos, action)
		pos = pos - (ndeleted or 0) + 1
	end
	local on_object = not visual and last_object_yanked and (range.start < range.finish)
	if on_object and pos > range.start then
		pos = range.start
	end
	local needs_separator = not visual and last_object_yanked and not textobject_includes_separator(last_object_yanked)
	local separator = ""
	if needs_separator and on_object then
		local sexp = env[vis.win].walker.sexp_at(range)
		if (sexp and sexp.indent and not sexp.is_line_comment) then
			separator = "\n"
		else
			separator = " "
		end
	end
	if last_object_yanked == "inner_line" then
		file:insert(pos, "\n")
	end
	file:insert(pos, clipboard.. ((on_object and needs_separator) and separator or ""))
	-- XXX: it is important NOT to "rewind forward", which would leave unparsed gaps,
	-- leading to incorrect structured actions:
	if env[vis.win].parser.tree.is_parsed(pos) then
		env[vis.win].parser.tree.rewind(pos)
	end
	local r = {start = pos, finish = pos}
	local _, parent = env[vis.win].walker.sexp_at(r, true)
	return env[vis.win].edit.refmt_at(parent, r) or pos
end

function H.O.join_line(_, range, pos)
	return env[vis.win].edit.join_line(range, pos) or pos
end

function H.O.wrap_round(_, range, pos)
	return env[vis.win].edit.wrap_round(range, pos, M.auto_square_brackets)
end

function H.O.wrap_square(_, range, pos)
	return env[vis.win].edit.wrap_square(range, pos, M.auto_square_brackets)
end

function H.O.wrap_curly(_, range, pos)
	return env[vis.win].edit.wrap_curly(range, pos, M.auto_square_brackets)
end

function H.O.wrap_doublequote(_, range, pos)
	return env[vis.win].edit.wrap_doublequote(range, pos, M.auto_square_brackets)
end

function H.O.meta_doublequote(_, range, pos)
	local escaped = env[vis.win].walker.escaped_at(range)
	if not escaped then
		return env[vis.win].edit.wrap_doublequote(range, pos, M.auto_square_brackets)
	elseif escaped.is_string then
		return env[vis.win].edit.close_and_newline(range, '"')
	end
	return pos
end

function H.O.wrap_comment(_, range, pos)
	return env[vis.win].edit.wrap_comment(range, pos)
end

function H.O.close_and_newline(_, range)
	return env[vis.win].edit.close_and_newline(range)
end

function H.O.open_next_line(_, range)
	return env[vis.win].edit.close_and_newline(range, "\n")
end

function H.O.raise_sexp(_, range, pos)
	return env[vis.win].edit.raise_sexp(range, pos) or pos
end

function H.O.splice_sexp(_, range, pos)
	return env[vis.win].edit.splice_anylist(range, pos) or pos
end

function H.O.cycle_wrap(_, range, pos)
	return env[vis.win].edit.cycle_wrap(range, pos)
end

function H.O.forward_slurp(_, range)
	local sexp = env[vis.win].walker.sexp_at(range)
	if sexp and sexp.is_empty and range.start == sexp.finish then
		range.finish = sexp.finish
	end
	return env[vis.win].edit.slurp_sexp(range, true)
end

function H.O.backward_slurp(_, range)
	local sexp = env[vis.win].walker.sexp_at(range)
	local keep_inside = 0
	if sexp and sexp.is_empty and range.start == sexp.finish then
		range.finish = sexp.finish
		keep_inside = 1
	end
	return env[vis.win].edit.slurp_sexp(range) - keep_inside
end

function H.O.forward_barf(_, range)
	return env[vis.win].edit.barf_sexp(range, true)
end

function H.O.backward_barf(_, range)
	return env[vis.win].edit.barf_sexp(range)
end

function H.O.split_sexp(_, range, pos)
	return env[vis.win].edit.split_anylist(range) or pos
end

function H.O.join_sexps(_, range, pos)
	return env[vis.win].edit.join_anylists(range) or pos
end

function H.O.convolute_sexp(_, range, pos)
	return env[vis.win].edit.convolute_lists(range) or pos
end

function H.O.transpose_sexps(_, range, pos)
	return env[vis.win].edit.transpose_sexps(range) or pos
end

function H.O.transpose_words(_, range, pos)
	return env[vis.win].edit.transpose_words(range) or pos
end

function H.O.transpose_chars(_, range, pos)
	return env[vis.win].edit.transpose_chars(range) or pos
end

local repl_fifo

function H.O.eval_defun(file, range, pos)
	if not M.repl_fifo or range.finish == range.start then return pos end
	if pos > range.finish then return pos end
	local unbalanced = env[vis.win].parser.tree.unbalanced_delimiters(range)
	if unbalanced and #unbalanced > 0 then return pos end
	local errmsg
	if not repl_fifo then
		repl_fifo, errmsg = io.open(M.repl_fifo, "a+")
	end
	if repl_fifo then
		repl_fifo:write(file:content(range), "\n")
		repl_fifo:flush()
	elseif errmsg then
		vis:info(errmsg)
	end
	return pos
end

-- flush any code stuck in the fifo, so starting a REPL after the file has been closed won't read old stuff in.
vis.events.subscribe(vis.events.WIN_CLOSE, function()
	if repl_fifo then
		repl_fifo:close()
		repl_fifo = io.open(M.repl_fifo, "w+")
		if repl_fifo then
			repl_fifo:close()
			repl_fifo = nil
		end
	end
end)

local function textobject_prep(func, name)
	return function(win, pos)
		local start, finish = func(win, range_by_pos(win, pos))
		if about_to_yank and start and finish and finish > start then
			last_object_yanked = name
		end
		return start, finish
	end
end

local function motion_prep(func)
	return function(win, pos)
		local exclusivity = (vis.mode == vis.modes.OPERATOR_PENDING or insert_mode) and 1 or 0
		local ret = func(win, {start = pos, finish = pos + (insert_mode and 0 or 1)}, pos, exclusivity)
		if about_to_yank and ret and ret ~= pos and ({[H.M.forward_up] = true, [H.M.backward_up] = true})[func] then
			last_object_yanked = "outer_list"
		end
		return ret or pos
	end
end

local function partially_autoselect(handler)
	return ({[H.M.beginning_of_sentence] = true, [H.M.end_of_sentence] = true})[handler]
end

local function prep_map(func, handler)
	return function()
		local anylist_partially_selected = false
		if M.autoselect then
			if vis.mode == vis.modes.INSERT then
				func()
				return
			end
			if partially_autoselect(handler) then
				if vis.mode == vis.modes.NORMAL then
					vis.mode = vis.modes.VISUAL
					anylist_partially_selected = true
				end
			elseif vis.mode ~= vis.modes.OPERATOR_PENDING then
				vis.mode = vis.modes.NORMAL
			end
		end
		func()
		if ({[H.M.prev_section] = true, [H.M.next_section] = true})[handler] then
			vis:feedkeys("<vis-window-redraw-top>")
		elseif M.autoselect and not anylist_partially_selected then
			vis.mode = vis.modes.VISUAL
			vis:textobject(R.T.outer_sexp)
			if ({[H.M.next_start] = true,
				[H.M.prev_start] = true,
				[H.M.prev_start_float] = true,
				[H.M.beginning_of_sentence] = true,
				[H.M.beginning_of_defun] = true,
				[H.M.prev_beginning_of_defun] = true,
				[H.M.backward_up] = true,
				[H.M.forward_down] = true})[handler] then
				vis:feedkeys("<vis-selection-flip>")
			end
		end
	end
end

function new.M(handler)
	local id = vis:motion_register(motion_prep(handler))
	local binding = id >= 0 and prep_map(function()
		vis:motion(id)
	end, handler)
	return id, binding
end

function new.T(handler, name)
	local id = vis:textobject_register(textobject_prep(handler, name))
	local binding = id >= 0 and prep_map(function()
		vis:textobject(id)
	end, handler)
	return id, binding
end

local function range_for(handler)
	if ({[H.O.yank] = true, [H.O.wrap_round] = true, [H.O.wrap_square] = true, [H.O.wrap_curly] = true,
		[H.O.wrap_doublequote] = true, [H.O.wrap_comment] = true, [H.O.meta_doublequote] = true})[handler]
		and mark_active() then
		vis:motion(R.M.mark)
	elseif ({[H.O.put_after] = true, [H.O.put_before] = true})[handler] then
		if last_object_yanked and not insert_mode then
			vis:textobject(textobject_includes_separator(last_object_yanked)
				and VIS_TEXTOBJECT_OUTER_LINE
				or R.T.outer_sexp)
		else
			vis:motion(insert_mode and VIS_MOVE_NOP or VIS_MOVE_CHAR_NEXT)
		end
	elseif ({[H.O.wrap_round] = true, [H.O.wrap_square] = true, [H.O.wrap_curly] = true,
		[H.O.wrap_doublequote] = true, [H.O.wrap_comment] = true, [H.O.meta_doublequote] = true})[handler] then
		if insert_mode then
			vis:motion(R.M.next_finish)
		else
			vis:textobject(R.T.outer_sexp)
		end
	elseif ({[H.O.eval_defun] = true})[handler] then
		vis:textobject(R.T.outer_paragraph)
	elseif ({[H.O.format] = true})[handler] then
		vis:textobject(R.T.inner_paragraph)
	elseif ({[H.O.splice_sexp] = true, [H.O.raise_sexp] = true, [H.O.cycle_wrap] = true,
		[H.O.split_sexp] = true, [H.O.join_sexps] = true,
		[H.O.forward_slurp] = true, [H.O.backward_slurp] = true,
		[H.O.forward_barf] = true, [H.O.backward_barf] = true,
		[H.O.convolute_sexp] = true,
		[H.O.close_and_newline] = true, [H.O.open_next_line] = true, [H.O.join_line] = true,
		[H.O.transpose_sexps] = true, [H.O.transpose_words] = true, [H.O.transpose_chars] = true})[handler] then
		vis:motion(insert_mode and VIS_MOVE_NOP or VIS_MOVE_CHAR_NEXT)
	end
end

function new.O(handler)
	local id = vis:operator_register(handler)
	local binding = id >= 0 and function()
		-- TODO: put this if/else block in the operator handlers, as here
		-- these statements won't get executed on a dot-repeat.
		if ({[H.O.yank] = true, [H.O.change] = true, [H.O.delete] = true})[handler] then
			vis.registers[insert_mode and "0" or vis.register or '"'] = {""}
			about_to_yank = true
			last_object_yanked = nil
		else
			-- XXX: this will not be reset on built-in and external operators:
			about_to_yank = false
		end
		local restore_visual
		if M.autoselect and vis.mode == vis.modes.VISUAL then
			restore_visual = true
		end
		if handler == H.O.join_line and vis.mode == vis.modes.VISUAL then
			vis.mode = vis.modes.NORMAL
		end
		begin_undo_action(handler == H.O.yank)
		vis:operator(id)
		if vis.mode == vis.modes.OPERATOR_PENDING then
			range_for(handler)
		end
		end_undo_action()
		if restore_visual and vis.mode == vis.modes.NORMAL then
			autoselect_region()
		end
	end
	return id, binding
end

local function depth_of(word, nodes, store)
	for i = #nodes, 1, -1 do
		if nodes[i] and nodes[i].is_list and not nodes[i].is_empty and nodes[i][1].text == word then
			table.insert(store, i)
			return true
		end
	end
end

local match_module = {
	scheme = function(win, pos)
		local indices, nodes = env[win].walker.sexp_path(range_by_pos(win, pos))
		if not (nodes[1] and nodes[1].is_list and not nodes[1].is_empty) then return end
		local node
		local cut = ""
		local depth = {}
		if nodes[1][1].text == "use-modules" then  -- Guile
			if #nodes[1] == 2 then
				node = nodes[1][2]
			elseif indices[2] and indices[2] < 2 then
				node = nodes[1][2]
			elseif indices[2] then
				node = nodes[1][indices[2]]
			end
		elseif indices[2] and indices[2] > 2 and nodes[1][1].text == "define-module" then  -- Guile
			if nodes[1][indices[2] - 1].text == "#:use-module" then
				node = nodes[1][indices[2]]
			elseif nodes[1][indices[2]].text == "#:use-module" then
				node = nodes[1][indices[2] + 1]
			end
		elseif depth_of("require", nodes, depth) then  -- Racket
			local d = depth[1]
			local smdepth = {}
			if #nodes[d] == 2 and (not nodes[d][2].is_list or nodes[d][2][1].text == "submod") then
				node = nodes[d][2]
			elseif indices[d + 1] and indices[d + 1] < 2 and (not nodes[d][2].is_list or nodes[d][2][1].text == "submod") then
				node = nodes[d][2]
			elseif depth_of("submod", nodes, smdepth) then
				local s = smdepth[1]
				node = nodes[s]
			end
			if node and node.is_list then cut = node[1].text end
		end
		return node and node.text
			:match("%(?".. cut .."[ \n]*([^)]+)")
			:gsub('"', "")  -- mostly because "." and ".." in Racket
			:gsub("'", "")
			:gsub("[ \n]+", "/")
	end,
	fennel = function(win, pos)
		local _, nodes = env[win].walker.sexp_path(range_by_pos(win, pos))
		if not (nodes[1] and nodes[1].is_list and not nodes[1].is_empty) then return end
		local node
		local depth = {}
		if depth_of("require", nodes, depth) then
			local d = depth[1]
			node = nodes[d][2]
		end
		return node and node.text
			:match("%:(.+)")
			:gsub("%.", "/")
	end,
}

vis.events.subscribe(vis.events.INIT, function()
	for kind, register_new in pairs(new) do
		for name, handler in pairs(H[kind]) do
			R[kind][name], B[kind][name] = register_new(handler, name)
		end
	end
	table.insert(keycfg, (require(cwd..".keys-vim")(B, H)))
	if M.compat then
		table.insert(keycfg, require(cwd..".keys-vim-compat")(B, H))
	end
	if M.emacs then
		table.insert(keycfg, (require(cwd..".keys-emacs")(B, H)))
	end
	if vis_goto_file then
		-- vis-goto-file is no good for parsing multiline includeexpr,
		-- provide our own helpers:
		for syntax, helper in pairs(match_module) do
			vis_goto_file(syntax, helper)
		end
	end
end)

local function seek(selection)
	return function(offset)
		selection.pos = offset
	end
end

vis.events.subscribe(vis.events.INPUT, function(char)
	if not vis_parkour(vis.win) then return end
	local ret
	local win = vis.win
	local winput = env[win].input
	local consumed_char = char:find(env[win].consumed_char)
	insert_mode = true
	-- XXX: no undo entry is saved for newlines, due to an implementation detail of begin_undo_action():
	-- it switches to NORMAL mode, and vis cleans up the indentation whitespace on the new empty line.
	if consumed_char and char ~= "\n" then begin_undo_action() end
	for i = #win.selections, 1, -1 do
		local selection = win.selections[i]
		local m = mark and win.file:mark_get(mark)
		local p = selection.pos
		local range = m and m ~= p and
			{start = math.min(m, p), finish = math.max(m, p)} or
			{start = p, finish = p}
		if char == "\n" then
			local sexp, parent = env[win].walker.sexp_at(range, true)
			if parent.is_root and (not sexp or (not sexp.is_comment and sexp.finish + 1 == range.start)) then
				local same_line = not not sexp
				if not sexp then
					local line, pos = selection.line, selection.pos
					-- XXX: the only call site without a skip argument (is_comment)
					-- was it intentional?
					local prev_finish = env[win].walker.finish_before(range)
					if prev_finish then
						seek(selection)(prev_finish)
						same_line = line == selection.line
						seek(selection)(pos)
					end
				end
				if sexp or same_line then
					eval_repl_line(win, range, selection.pos)
				end
			end
		end
		ret = winput.insert(range, seek(selection), char, M.auto_square_brackets)
	end
	-- XXX: this ridiculous sequence is necessary to save an undo history entry
	-- for autopairs insertion _distinct_ from the regular characters afterwards.
	if consumed_char and char ~= "\n" then end_undo_action() begin_undo_action() end_undo_action() end
	insert_mode = false
	mark = nil
	return ret
end)

-- XXX: other plugins can check this to avoid collisions.
-- (in Vis, mapping and operator handlers can't be composed)
function vis_parkour(win)
	return supports[win.syntax]
end

return M
