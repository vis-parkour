vis-parkour is a structured editing plugin for the [Vis editor](https://github.com/martanne/vis),
based on the [parkour library](https://repo.or.cz/lisp-parkour.git).

# Configuration

In `~/.config/vis/visrc.lua`:

    local pk = require'vis-parkour'

    -- optional (example values):
    pk.auto_square_brackets = true
    pk.lispwords.scheme.lambda = 0
    pk.repl_fifo = os.getenv('HOME')..'/.repl_fifo'
    pk.emacs = true
    pk.compat = true
    pk.autoselect = true

## `auto_square_brackets`

Rewrites any delimiter to a square bracket at certain locations.  
Has effect only on Scheme; for Clojure and Fennel this behaviour is always on.  
Works both when inserting and wrapping.  
The locations are not configurable ATM, but are grouped by language in the code - see, for example, the [Fennel config](parkour/cfg-fennel.lua#l24).

## `lispwords`_`.dialect.word`_

The plugin comes with a set of indentation rules for each supported file type,
but they are incomplete and sometimes will be wrong (due to multiple possible dialects under a single file type).  
The `lispwords` table allows customizing those rules by setting the desired number of distinguished arguments
a function/macro has. (`0` is like `defun` in Emacs.)  
As an example, see the built-in indent numbers [for Scheme](parkour/cfg-scheme.lua#l11).

## `repl_fifo`

This option can be set to the path of a named pipe from which a REPL (or anything, really) can read input.  
Pressing `Enter` will send to this pipe either the current paragraph or the visual selection.
This works in INSERT mode, too, but only when the cursor is after the last character of a paragraph.  
Since REPL commands fit the criteria for paragraph (a top-level S-expression), they get sent as well.

## `emacs`

The plugin comes with two key themes - [emacs](keys-emacs.lua) and [vim](keys-vim.lua).  
`vim` is always enabled.  
Setting this option to true will enable the `emacs` key theme, too. (There is no overlap between their key mappings; moreover, the `emacs` theme consists entirely of INSERT-mode mappings).

## `compat`

Setting this option to true will swap some of the motions in the native key theme with ones more resembling the built-in vis/vim motions.  
See [vim-compat](keys-vim-compat.lua).

__Note:__ The linked key themes are the only "documentation" on the available keyboard shortcuts ATM.

## `autoselect` ("Kakoune mode")

Setting this option to true will make every motion that has an associated S-expression textobject to automatically select it.  
INSERT mode can be directly switched to from VISUAL mode; `i` will put the cursor before the _selection_, and `a` - after it.

__Note:__ the Kakoune reference is mainly wrt the noun-verb order; this is not an attempt to recreate Kakoune's own key mappings.
(Adding a new key theme could do that, but only to an extent.)

# Usage

1. Motions and textobjects:  
    - a bigword/block is a list
    - a sentence is _the current_ list/string/comment  
    Note that sentence motions are exclusive in OPERATOR-PENDING mode.
    - a word is any S-expression - an atom or, if the cursor is on a delimiter, a list/string/comment  
    Note that, when the cursor is on a list delimiter, a block textobject will select its _parent_ list.  
    This is not compatible with vis/vim, but makes motions and textobjects consistent with each other:  
        - using a bigword/block/sentence _always_ leads to depth change
        - using a word _never_ leads to depth change
    - a paragraph is a top-level S-expression
    - word motions skip over comments, unless the cursor is already in a comment
2. Inner/outer textobjects:  
    This only makes a difference for delimited objects - lists, strings, comments.
3. Objectwise deletion:  
    When removing an S-expression that is part of a list, the adjacent whitespace is removed
    if you use `d`, and left intact if you use `c` (or are in INSERT mode).  
    This holds true even when using a motion or inner textobject, or when in VISUAL mode.  
    Any removed adjacent whitespace will not be copied to the register (but see the next item).
4. Objectwise pasting:  
    If the last `y`/`d`/`c` was used with a textobject, `p` will paste after the current S-expression,
    and `P` - before it.  
    An appropiate type of whitespace (space or newline) will be added as well.  
    (This generalizes over the idea of linewise pasting)
5. autosplicing/autowrapping `d` and `y`:  
    `yE`/`yB` wraps the copied half of the list in the same delimiters as the original (including any reader macro prefix).  
    `dE`/`dB` does so too, and splices the original list.
6. Pressing `v` while in VISUAL mode expands the selection by semantic units.

For some of the supported dialects (Guile, Racket, and Fennel, currently), the plugin cooperates with
[vis-goto-file](https://repo.or.cz/vis-goto-file.git), and parses include expressions for it.

# Bugs

Vis lacks a "buffer-changed" kind of event, so the plugin cannot detect any changes other than
[those it caused itself](keys-vim.lua#l24).  
This can be a problem if you use custom operators, or rotate visual selections (maybe other things, too).  
When a change goes undetected, the internal parse tree grows out of sync with the actual text,
word boundaries are no longer where the plugin expects them to be, and various weird and hard-to-reproduce issues may occur.  
A work-around is to call undo and redo right after an external change.

Count argument is not implemented yet for operators and textobjects (but works for motions).
OTOH, dot-repeating and semantic expansion of the visual selection do work and can often be good enough a substitute.

Dot-repeating a deletion will concatenateallthepieces in the register instead of overwriting the last piece.
