return function(B, H)
return {
	M = {
		 w  = B.M.next_start_float,
		 b  = B.M.prev_start_float,
		 e  = B.M.next_finish_float,
		 ge = B.M.prev_finish_float,
		 ['}'] = B.M.next_beginning_of_defun,
		 ['{'] = B.M.prev_beginning_of_defun,
		 ['<S-Right>'] = B.M.next_finish,
		 ['<S-Left>']  = B.M.prev_start,
	},
	imap = {
		 ['<S-Right>'] = B.M.next_finish,
		 ['<S-Left>']  = B.M.prev_start,
		 ['<C-w>'] = H.I.backward_kill_word,
	},
}
end
