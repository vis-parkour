-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local lispwords = {
	['do'] = -1,
	['eval-compiler'] = -1,
	['if'] = -1,

	['doto'] = 1,
	['each'] = 1,
	['fn'] = 1,
	['for'] = 1,
	['lambda'] = 1,
	['let'] = 1,
	['match'] = 1,
	['when'] = 1,
	['while'] = 1,
	['with-open'] = 1,
	['λ'] = 1,

	['macro'] = 2,
}

local squarewords = {
	not_optional = true,

	['each'] = 1,
	['for'] = 1,
	['let'] = 1,
	['with-open'] = 1,

	['fn'] = 2,
	['lambda'] = 2,
	['macro'] = 2,
	['λ'] = 2,
}

local l = require'lpeg'
local P, S = l.P, l.S

local macro_prefix = S"#,`'"

local weak_prefix = P'#'

local delimiters = {
	['('] = ')',
	['{'] = '}',
	['['] = ']',
	['"'] = '"',
	[';'] = '\n',
}

return {
	prefix = macro_prefix,
	weak_prefix = weak_prefix,
	opposite = delimiters,
	lispwords = lispwords,
	squarewords = squarewords,
}
