-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local l = require'lpeg'
local P, S, R, V, C, Cb, Cc, Cg, Cmt, Cp, Ct = l.P, l.S, l.R, l.V, l.C, l.Cb, l.Cc, l.Cg, l.Cmt, l.Cp, l.Ct

local M = {}

local hspace = S" \t"
local newline = S"\r\n"

local function incr(index, offset) return index + offset - 1 end
local function m1(pos) return pos - 1 end
local function neg(pos) return -pos end
local function past(_, position, pos) return position <= pos end
local function startof(element) return element.start end
local function _start(patt) return Cg(patt, "start") end
local function _finish(patt) return Cg(patt, "finish") end
local _string = Cg(Cc(true), "is_string")
local _char = Cg(Cc(true), "is_char")
local _comment = Cg(Cc(true), "is_comment")
local _line_comment = Cg(Cc(true), "is_line_comment")
local function delimited_range(d)
	return Cg(d, "d") * (P(1) - "\\" - d + P"\\" * 1)^0 * d
end
local function nested_pair(d1, d2)
	return P{Cg(d1, "d") * (P(1) - d1 - d2 + V(1))^0 * d2}
end

local function purge(base, sexps)
	if not base then return sexps end
	for _, list in pairs(sexps) do
		for i = #list, 1, -1 do
			if list[i].start >= base then
				table.remove(list)
			else
				break
			end
		end
	end
	return sexps
end

local function append(old, new)
	for name, list in pairs(new) do
		for _, element in ipairs(list) do
			table.insert(old[name], element)
		end
		if old[name].is_root then
			-- XXX: check the length first and then access [1].
			-- Otherwise the autovivification of [1] in a file with only blanks
			-- will trigger an infinite recursion.
			-- TODO: get rid of this trick altogether and instead make the parser set [1].indent.
			local first = #old[name] > 0 and old[name][1]
			-- XXX: this assumes that the first sexp starts at column 0.
			-- if not true, _only_ the first sexp will have wrong indent.
			if first and not first.indent then first.indent = 0 end
		end
	end
	local last = #old.tree ~= 0 and old.tree[#old.tree]
	old.tree.first_invalid = last and (last.finish or last.start) + 1 or 0
	return old
end

local function make_driver(read, parse, before, sexps)
	return function(advance)
		local base
		if sexps.tree.first_invalid then
			local last_parsed = #sexps.tree ~= 0 and sexps.tree[#sexps.tree].finish + 1 or 0
			if sexps.tree.first_invalid < last_parsed then
				local nearest, n = before(sexps.tree, sexps.tree.first_invalid, startof)
				local second_nearest = n and sexps.tree[n - 1]
				local has_eol = second_nearest and second_nearest.is_line_comment
				local nearest_finish = second_nearest and second_nearest.finish + (has_eol and 0 or 1)
				local nearest_start = nearest and nearest.start
				base = nearest_finish or nearest_start or sexps.tree.first_invalid
			else
				local last = sexps.tree[#sexps.tree]
				local has_eol = last and last.is_line_comment
				base = has_eol and last.finish or sexps.tree.first_invalid
			end
		end
		local new
		local chunk_size = 4 * 1024
		local tries = 1
		local content = ""
		local from = base
		local len = advance > 0 and advance - base + chunk_size or chunk_size
		repeat
			local chunk, further = read(from, len)
			if not chunk then break end
			content = content .. chunk
			new = parse(content, advance, base)
			from = from + len
			tries = tries + 1
			len = chunk_size * 2^(tries - 1)
		until not new.tree.unbalanced and (advance > 0 or advance < 0 and #new.tree >= math.abs(advance))
			or not further
		return append(purge(base, sexps), new or {})
	end
end

local function tag_next_node(t, i, pos)
	local j = i
	repeat
		j = j + 1
	until type(t[j]) == "table" or not t[j]
	local nxt = t[j]
	if pos >= 0 then
		if nxt and not nxt.indent then
			nxt.indent = nxt.start - pos - 1
		end
	else
		nxt.section = true
	end
end

local function make_parser(prefix, weak_prefix, opposite, d1, d2, D1, D2, atom_node, list_node, quasilist_node, lispwords, squarewords)
	local function atom_methodify(t)
		return setmetatable(t, (t.d and not t.is_list) and quasilist_node or atom_node)
	end
	local function extract_breaks(t)
		for i = #t, 1, -1 do
			local node = t[i]
			if type(node) == "number" then
				tag_next_node(t, i, node)
				table.remove(t, i)
			elseif node.is_line_comment then
				tag_next_node(t, i, node[1])
				table.remove(node)
			end
		end
		return t
	end

	local function get_lispwords(head)
		return lispwords[head] or lispwords[0] and lispwords[0]:match(head)
	end

	local function get_squarewords(head)
		return squarewords[head] or squarewords[0] and squarewords[0]:match(head)
	end

	-- TODO: find a way without storing this key at all
	local function delete_operator(t)
		t.operator = nil
		return t
	end

	return function(content, advance, offset)
		local tree, esc = nil, {}
		local I = Cp() * Cc(offset) / incr
		local I1 = I / m1
		local opening = _start(I) * Cg(prefix^1, "p")^-1 * d1
		local closing = _finish(I) * d2
		local lone_prefix = _start(I) * Cg(prefix^1, "p") * -#P(d1 + "|") * _finish(I1)
		local unbalanced
		local function debalance(t) unbalanced = true return t end
		local function collect_escaped(sexp)
			if sexp.is_string or sexp.is_comment or sexp.is_char then
				table.insert(esc, sexp)
			end
			return sexp
		end
		local char = ("\\" * (P"alarm" + "backspace" + "delete" + "escape"
				+ "newline" + "null" + "return" + "space" + "tab")
				+ "\\x" * R("09", "AF", "af")^1
				+ "\\" * P(1)) * _char
		local dq_string, multi_esc = delimited_range('"'), delimited_range("|")
		local stringy = (opposite["|"] and (multi_esc + dq_string) or dq_string) * _string
		local nline = I * newline
		local blank = nline + hspace + "\f"
		local lcd = opposite["\n"]
		local lc_level = lcd * #-P(lcd) + P(lcd)^1 * hspace^-1
		local line_comment = Cg(lc_level, "d") * (1 - nline)^0 * nline * _comment * _line_comment
		local bcd1, bcd2
		for o, c in pairs(opposite) do
			-- XXX: here I assume that #| is the only multi-char delimiter,
			-- and that it means block comment
			if #o > 1 then
				bcd1, bcd2 = o, c
				break
			end
		end
		local block_comment = bcd1 and bcd2 and nested_pair(bcd1, bcd2) * _comment
		local comment = block_comment and (block_comment + line_comment) or line_comment
		local incomplete_comment = bcd1 and (lc_level + bcd1) or lc_level
		local symbolish = 1 - D1 - D2 - blank - char - stringy - comment - prefix
		local symbol = weak_prefix and (symbolish + weak_prefix)^1 or symbolish^1
		local atom = Ct(_start(I) * (
			comment
			+ Cg(prefix^1, "p")^-1 * (
				stringy
				+ char
				+ symbol
			)) * _finish(I1)) / atom_methodify / collect_escaped
			+ Ct(lone_prefix)
		local operator = squarewords and
			Ct(_start(I) * (
			Cg(symbolish^1, "operator")
			* Cg(Cb("operator") / get_lispwords, "last_distinguished")
			* Cg(Cb("operator") / get_squarewords, "auto_square")
			) * _finish(I1)) / delete_operator / atom_methodify
			or
			Ct(_start(I) * (
			Cg(symbolish^1 / get_lispwords, "last_distinguished")
			) * _finish(I1)) / atom_methodify
		local list = P{Ct(opening * blank^0 * operator^-1 * (atom + blank^1 + V(1))^0 * closing) *
			Cc(list_node) / setmetatable / extract_breaks}
		local lone_delimiter = Ct(_start(I) * (D1 + D2 + incomplete_comment) * _finish(I1)) / debalance
		local section_break = I * "\f" / neg
		local sexp = (section_break + blank)^0 * (list + atom + lone_delimiter)
		if advance ~= 0 then
			local top_level = Ct(advance < 0
				and sexp^advance
				or (Cmt(Cc(advance - offset), past) * sexp)^0)
			tree = P{top_level / extract_breaks}:match(content)
			tree.unbalanced = unbalanced
		end
		return {tree = tree, escaped = esc}
	end
end

local opposite_fallback = {
	__index = function(t, delimiter)
		local opening = t["\n"]
		return delimiter and delimiter:find("^" .. opening) and t[opening]
	end
}

local function catchup(tree, range)
	if range.finish and range.finish >= tree.first_invalid then
		tree.parse_to(range.finish + 1)
	end
end

function M.new(cfg, node, read)
	local opposite = setmetatable(cfg.opposite, opposite_fallback)
	local sexps = {
		tree = {
			first_invalid = 0,
			is_root = true,
		},                    -- (sorted) tree of sexps
		escaped = {},         -- sorted list of strings and comments (redundant, for faster search)
	}
	local opening, closing, reverse = {}, {}, {}
	for o, c in pairs(opposite) do
		if S"([{":match(o) then
			table.insert(opening, o)
			table.insert(closing, c)
		end
		if c ~= o and #c == 1 then  -- XXX: don't reverse block comments and strings
			reverse[c] = o
		end
	end
	for c, o in pairs(reverse) do
		opposite[c] = o
	end
	local D1 = S(table.concat(opening))
	local d1 = Cg(D1, "d")
	local d2 = Cmt(Cb("d") * C(1), function(_, _, o, c) return opposite[o] == c end)
	local D2 = S(table.concat(closing))
	local atom_node, list_node, quasilist_node = node.atom, node.list, node.quasilist(opposite)
	local parse = make_parser(cfg.prefix, cfg.weak_prefix, opposite, d1, d2, D1, D2, atom_node, list_node, quasilist_node, cfg.lispwords, cfg.squarewords)
	local drive = make_driver(read, parse, node._before, sexps)
	local root_methods = node.root(opposite)
	setmetatable(sexps.tree, {
		__index = function(t, key)
			if type(key) == "number" then
				if #t < key then t.parse_to(#t - key) end
				return rawget(t, key)
			elseif root_methods[key] then
				return root_methods[key](t)
			elseif key == "parse_to" then
				return drive
			end
		end
	})
	setmetatable(sexps.escaped, {
		__index = {
			around = function(range)
				catchup(sexps.tree, range)
				return node._around(sexps.escaped, range)
			end
		}
	})
	return {tree = sexps.tree, escaped = sexps.escaped, opposite = opposite}, d1, D2
end

return M
