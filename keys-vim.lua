return function(B, H)
return {
	M = {
		 w  = B.M.next_start,
		 b  = B.M.prev_start,
		 e  = B.M.next_finish,
		 ge = B.M.prev_finish,
		 W  = B.M.forward_down,
		 B  = B.M.backward_up,
		 E  = B.M.forward_up,
		 gE = B.M.backward_down,
		 Q  = B.M.backward_down,  -- XXX: easier to repeat (no need to release and press Shift)
		 ['}'] = B.M.end_of_defun,
		 ['{'] = B.M.beginning_of_defun,
		 [')'] = B.M.end_of_sentence,
		 ['('] = B.M.beginning_of_sentence,
		 [']]'] = B.M.next_section,
		 ['[['] = B.M.prev_section,
		 ['<S-Right>'] = B.M.next_finish_float,
		 ['<S-Left>']  = B.M.prev_start_float,
		 ['$'] = B.M.line_end,
		 ['0'] = B.M.line_begin,
	},
	O = {
		 gS = B.O.split_sexp,
		 gJ = B.O.join_sexps,
		 ['g}'] = B.O.forward_barf,
		 ['g)'] = B.O.forward_slurp,
		 ['g{'] = B.O.backward_barf,
		 ['g('] = B.O.backward_slurp,
		 ['g.'] = B.O.raise_sexp,
		 ['g?'] = B.O.convolute_sexp,
		 ['g@'] = B.O.splice_sexp,
		 ['gw('] = B.O.wrap_round,
		 ['gW']  = B.O.wrap_round,
		 ['gw['] = B.O.wrap_square,
		 ['gw{'] = B.O.wrap_curly,
		 ['gw"'] = B.O.wrap_doublequote,
		 ['gw;'] = B.O.wrap_comment,
		 ['<Enter>'] = B.O.eval_defun,
		 gT = B.O.transpose_sexps,
		 gt = B.O.transpose_words,
		 ['='] = B.O.format,
		 gc = B.O.cycle_wrap,
		 J = B.O.join_line,
		 d = B.O.delete,
		 c = B.O.change,
		 y = B.O.yank,
		 p = B.O.put_after,
		 P = B.O.put_before,
	},
	T = {
		 aw = B.T.outer_sexp,
		 iw = B.T.inner_sexp,
		 aW = B.T.outer_list,
		 iW = B.T.inner_list,
		 ab = B.T.outer_list,
		 ib = B.T.inner_list,
		 as = B.T.outer_sentence,
		 is = B.T.inner_sentence,
		 ap = B.T.outer_paragraph,
		 ip = B.T.inner_paragraph,
		 ['a"'] = B.T.outer_string,
		 ['i"'] = B.T.inner_string,
		 ['a;'] = B.T.outer_comment,
		 ['i;'] = B.T.inner_comment,
		 ['a('] = B.T.outer_round,
		 ['i('] = B.T.inner_round,
		 ['a['] = B.T.outer_square,
		 ['i['] = B.T.inner_square,
		 ['a{'] = B.T.outer_curly,
		 ['i{'] = B.T.inner_curly,
		 al = B.T.outer_line,
		 il = B.T.inner_line,
	},
	imap = {
		 ['<S-Right>'] = B.M.next_finish_float,
		 ['<S-Left>']  = B.M.prev_start_float,
		 ['<Delete>']    = H.I.forward_delete,
		 ['<Backspace>'] = H.I.backward_delete,
		 ['<C-w>'] = H.I.backward_kill_sexp,
		 ['<C-u>'] = H.I.backward_kill_line,
		 ['<Tab>'] = B.O.format,
	},
	nmap = {
		 u         = H.A.undo,
		 ['<C-r>'] = H.A.redo,
		 ['<C-n>'] = H.A.select_sexp,
	},
	raw = function(win)
		local vis = vis
		local function nmap(lhs, rhs)
			win:map(vis.modes.NORMAL, lhs, rhs)
		end
		nmap('x', 'dl')
		nmap('dd', 'dal')
		nmap('cc', 'cil')
		nmap('yy', 'yal')
		nmap('O', '<vis-motion-line-begin><vis-motion-char-prev><vis-mode-insert><Enter>')
		win:unmap(vis.modes.NORMAL, '$')
		win:unmap(vis.modes.NORMAL, '0')
		win:unmap(vis.modes.VISUAL, '<C-n>')
	end
}
end
